package com.coffee.backoffice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class CoffeeMachineAlreadyExistsException extends RuntimeException {
    public CoffeeMachineAlreadyExistsException(){
        super();
    }

    public CoffeeMachineAlreadyExistsException(String message) {
        super(message, null);
    }
}
