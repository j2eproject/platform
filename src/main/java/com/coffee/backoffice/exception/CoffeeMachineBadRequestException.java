package com.coffee.backoffice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class CoffeeMachineBadRequestException extends RuntimeException {
    public CoffeeMachineBadRequestException(){
        super();
    }

    public CoffeeMachineBadRequestException(String message) {
        super(message, null);
    }
}
