package com.coffee.backoffice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class CoffeeMachineNotFoundException extends RuntimeException {
    public CoffeeMachineNotFoundException(){
        super();
    }

    public CoffeeMachineNotFoundException(String message) {
        super(message, null);
    }
}
