package com.coffee.backoffice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class CoffeeMachineForbiddenException extends RuntimeException {
    public CoffeeMachineForbiddenException(){
        super();
    }

    public CoffeeMachineForbiddenException(String message) {
        super(message, null);
    }
}
