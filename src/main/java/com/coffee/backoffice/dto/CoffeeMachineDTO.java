package com.coffee.backoffice.dto;

import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Min;

public class CoffeeMachineDTO {
    private Long id;
    @Min(value = 1, message = "Le port doit être d'une valeur minimale de 1")
    private Integer port;
    @URL
    private String url;
    private String version;
    private boolean isActive;
    private boolean isOutOfStock;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isOutOfStock() {
        return isOutOfStock;
    }

    public void setOutOfStock(boolean outOfStock) {
        isOutOfStock = outOfStock;
    }
}