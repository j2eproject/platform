package com.coffee.backoffice.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class LoginDTO {
    @NotNull(message = "Le champ ne peut pas êtres vide")
    @NotEmpty(message = "Le champ ne peut pas être vide")
    private String mail;
    @NotNull(message = "Le champ ne peut pas êtres vide")
    @NotEmpty(message = "Le champ ne peut pas être vide")
    private String password;

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
