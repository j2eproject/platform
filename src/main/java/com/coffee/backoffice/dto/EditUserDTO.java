package com.coffee.backoffice.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class EditUserDTO {
    @NotNull(message = "Le champ ne peut pas êtres vide")
    @NotEmpty(message = "Le champ ne peut pas être vide")
    public String lastname;
    @NotNull(message = "Le champ ne peut pas êtres vide")
    @NotEmpty(message = "Le champ ne peut pas être vide")
    public String firstname;
    public String role;

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}