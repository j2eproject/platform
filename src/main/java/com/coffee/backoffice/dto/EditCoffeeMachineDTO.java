package com.coffee.backoffice.dto;

import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Min;

public class EditCoffeeMachineDTO {
    @Min(value = 1, message = "Le port doit être d'une valeur minimale de 1")
    private Integer port;
    @URL
    private String url;

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}