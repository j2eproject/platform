package com.coffee.backoffice.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class EditPasswordDTO {
    @NotNull(message = "Le champ ne peut pas êtres vide")
    @NotEmpty(message = "Le champ ne peut pas être vide")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$",
            message = "Le champ mot de passe doit respecter la règle de mot de passe")
    public String password;
    @NotNull(message = "Le champ ne peut pas êtres vide")
    @NotEmpty(message = "Le champ ne peut pas être vide")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$",
            message = "Le champ confirmation du mot de passe doit respecter la règle de mot de passe")
    public String confirmPassword;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}