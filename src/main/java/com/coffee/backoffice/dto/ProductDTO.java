package com.coffee.backoffice.dto;

public class ProductDTO {
    public Long id;
    private String libelle;
    private Integer quantity;
    private String coffeemachine;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getCoffeemachine() {
        return coffeemachine;
    }

    public void setCoffeemachine(String coffeemachine) {
        this.coffeemachine = coffeemachine;
    }
}
