package com.coffee.backoffice.dto;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataDTO {
    private Integer port;
    private String version;
    private List<Map.Entry<String, Object>> products;

    public DataDTO() {
        products = new ArrayList<>();
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public  List<Map.Entry<String, Object>> getProducts() {
        return products;
    }

    public void setProducts (List<Map.Entry<String, Object>> products) {
        this.products = products;
    }
}
