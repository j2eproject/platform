package com.coffee.backoffice.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class CreateProductDTO {
    @NotNull(message = "Le champ ne peut pas être vide")
    @NotEmpty(message = "Le champ ne peut pas être vide")
    private String libelle;
    @Min(0)
    private Integer quantity;

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
