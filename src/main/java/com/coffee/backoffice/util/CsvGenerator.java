package com.coffee.backoffice.util;

import com.coffee.backoffice.dto.ProductDTO;
import com.coffee.backoffice.dto.UserDTO;
import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvException;
import java.io.PrintWriter;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CsvGenerator {
    private static final Logger LOGGER = LoggerFactory.getLogger(CsvGenerator.class);

    public static void usersReport(PrintWriter writer, List<UserDTO> users)  {

        try {

            ColumnPositionMappingStrategy mapStrategy = new ColumnPositionMappingStrategy();

            mapStrategy.setType(UserDTO.class);
            mapStrategy.generateHeader();

            String[] columns = new String[]{"id", "mail", "firstname", "lastname", "role"};
            mapStrategy.setColumnMapping(columns);


            StatefulBeanToCsv btcsv = new StatefulBeanToCsvBuilder(writer)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .withMappingStrategy(mapStrategy)
                    .withSeparator(',')
                    .build();

            btcsv.write(users);

        } catch (CsvException ex) {

            LOGGER.error("Error mapping Bean to CSV", ex);
        }
    }

    public static void productsReport(PrintWriter writer, List<ProductDTO> products)  {

        try {

            ColumnPositionMappingStrategy mapStrategy = new ColumnPositionMappingStrategy();

            mapStrategy.setType(ProductDTO.class);
            mapStrategy.generateHeader();

            String[] columns = new String[]{"id", "libelle", "quantity", "coffeemachine"};
            mapStrategy.setColumnMapping(columns);


            StatefulBeanToCsv btcsv = new StatefulBeanToCsvBuilder(writer)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .withMappingStrategy(mapStrategy)
                    .withSeparator(',')
                    .build();

            btcsv.write(products);

        } catch (CsvException ex) {

            LOGGER.error("Error mapping Bean to CSV", ex);
        }
    }

}
