package com.coffee.backoffice.service;

import com.coffee.backoffice.exception.UserAlreadyExistsException;
import com.coffee.backoffice.exception.UserBadRequestException;
import com.coffee.backoffice.exception.UserNotFoundException;
import com.coffee.backoffice.model.User;
import com.coffee.backoffice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User save(User user) {
        if (userRepository.findByMail(user.getMail()).isPresent()) {
            throw new UserAlreadyExistsException("Un utilisateur avec le mail " + user.getMail() + " existe déjà");
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRole(User.USER);
        return userRepository.save(user);
    }

    @Override
    public User findById(Long id) {
        return userRepository
                        .findById(id)
                        .orElseThrow(() -> new UserNotFoundException("Aucun utilisateur d'id " + id + " existe"));
    }

    @Override
    public User findByMail(String mail) {
        return userRepository
                .findByMail(mail)
                .orElseThrow(() -> new UserNotFoundException("Aucun d'utilisateur avec le mail " + mail + " existe"));
    }

    @Override
    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Objects.requireNonNull(username);

        Optional<User> oUser = userRepository.findByMail(username);

        if (!oUser.isPresent()) {
            throw new UsernameNotFoundException("Aucun utilisateur avec comme mail + " + username);
        }

        User user = oUser.get();
        return  org.springframework.security.core.userdetails.User
                .withUsername(user.getMail())
                .password(user.getPassword())
                .roles(user.getRole())
                .build();
    }

    @Override
    public void delete(Long id) {
        Optional<User> oUser = userRepository.findById(id);

        if (!oUser.isPresent()) {
            throw new UserNotFoundException("Aucun utilisateur d'identifiant " + id);
        }
        userRepository.delete(oUser.get());
    }

    @Override
    public User update(Long id, User user) {
        return userRepository.findById(id)
                .map(x -> {
                    x.setFirstname(user.getFirstname());
                    x.setLastname(user.getLastname());
                    return userRepository.save(x);
                })
                .orElseThrow(() -> new UserNotFoundException("Aucun utilisateur d'id " + id + " existe"));
    }

    @Override
    public User updateWithRole(Long id, User user) {
        if (!(user.getRole().equals("USER") || user.getRole().equals("ADMIN"))) {
            throw new UserBadRequestException("Le rôle " + user.getRole() + " n'est pas reconnu");
        }

        return userRepository.findById(id)
                .map(x -> {
                    x.setFirstname(user.getFirstname());
                    x.setLastname(user.getLastname());
                    x.setRole(user.getRole());
                    return userRepository.save(x);
                })
                .orElseThrow(() -> new UserNotFoundException("Aucun utilisateur d'id " + id + " existe"));
    }

    @Override
    public void updatePassword(Long id, String password, String confirmPassword) {
        if (!password.equals(confirmPassword)) {
            throw new UserBadRequestException("Les mots de passe ne correspondent pas");
        }

        userRepository.findById(id)
                .map(x -> {
                    x.setPassword(passwordEncoder.encode(password));
                    return userRepository.save(x);
                })
                .orElseThrow(() -> new UserNotFoundException("Aucun utilisateur d'id " + id + " existe"));
    }
}