package com.coffee.backoffice.service;

import com.coffee.backoffice.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    User save(User user);

    User findByMail(String mail);

    User findById(Long id);

    Iterable<User> findAll();

    void delete(Long id);

    User update(Long id, User user);

    User updateWithRole(Long id, User user);

    void updatePassword(Long id, String password, String confirmPassword);
}

