package com.coffee.backoffice.service;

import com.coffee.backoffice.exception.CoffeeMachineBadRequestException;
import com.coffee.backoffice.exception.CoffeeMachineNotFoundException;
import com.coffee.backoffice.exception.ProductNotFoundException;
import com.coffee.backoffice.model.CoffeeMachine;
import com.coffee.backoffice.model.Product;
import com.coffee.backoffice.repository.ProductRepository;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product save(Product product) {
        try {
            HttpResponse<JsonNode> jsonResponse
                    = Unirest.post(product.getCoffeeMachine().getUrl() + ":" + product.getCoffeeMachine().getPort()
                    + "/product")
                    .body("{\"productName\":\"" + product.getLibelle() + "\", " +
                           "\"quantity\":\"" + product.getQuantity()   + "\"}")
                    .asJson();

            if (jsonResponse.getStatus() == 200) {
                if (product.getCoffeeMachine().getVersion().equals("1.1")) {
                     Optional<Product> optionalProduct = ((List<Product>)productRepository
                             .findAllByCoffeeMachine(product.getCoffeeMachine()))
                             .stream()
                             .filter(o -> o.getLibelle().equals(product.getLibelle()))
                             .findFirst();

                     if (optionalProduct.isPresent()) {
                         Product existingProduct = optionalProduct.get();
                         existingProduct.setQuantity(existingProduct.getQuantity() + product.getQuantity());
                         return productRepository.save(existingProduct);
                     } else {

                         return productRepository.save(product);
                     }
                } else {
                    return productRepository.save(product);
                }
            }
        }
        catch (Exception e) {
            throw new CoffeeMachineNotFoundException("Erreur lors de l'ajout du produit");
        }
        return null;
    }

    @Override
    public Product saveData(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product findById(Long id) {
        return productRepository
                .findById(id)
                .orElseThrow(() -> new ProductNotFoundException(
                        "Aucun produit d'identifiant " + id + " existe"));
    }

    @Override
    public Iterable<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public void delete(String libelle, int quantity, CoffeeMachine machine) {
        List<Product> products = (List<Product>) productRepository.findAllByCoffeeMachine(machine);
        Optional<Product> oProduct = products.stream()
                .filter(o -> o.getLibelle().equals(libelle))
                .findFirst();
        if (!oProduct.isPresent()) {
            throw new ProductNotFoundException("Aucun produit de libelle " + libelle);
        }
        Product product = oProduct.get();
        try {
            HttpResponse<JsonNode> jsonResponse
                    = Unirest.delete(product.getCoffeeMachine().getUrl() + ":" + product.getCoffeeMachine().getPort()
                    + "/product")
                    .body("{\"productName\":\"" + libelle + "\", " +
                            "\"quantity\":\"" + quantity   + "\"}")
                    .asJson();

            if (jsonResponse.getStatus() == 200) {
                if (product.getCoffeeMachine().getVersion().equals("1.1")) {
                    int newQuantity = product.getQuantity() - quantity;
                    product.setQuantity(newQuantity > 0 ? newQuantity : 0);
                } else {
                    productRepository.deleteByLibelle(product.getLibelle());
                }
            }
        } catch (Exception e) {

            throw new CoffeeMachineNotFoundException("Erreur lors de la suppression du produit");
        }
    }

    @Override
    public long deleteAllByCoffeeMachine(CoffeeMachine coffeeMachine) {
        return productRepository.deleteAllByCoffeeMachine(coffeeMachine);
    }

    @Override
    public Iterable<Product> findAllByCoffeeMachine(CoffeeMachine coffeeMachine) {
        return productRepository.findAllByCoffeeMachine(coffeeMachine);
    }

}