package com.coffee.backoffice.service;

import com.coffee.backoffice.exception.*;
import com.coffee.backoffice.model.CoffeeMachine;
import com.coffee.backoffice.model.Product;
import com.coffee.backoffice.model.User;
import com.coffee.backoffice.repository.CoffeeMachineRepository;
import com.coffee.backoffice.repository.ProductRepository;
import com.coffee.backoffice.repository.UserRepository;
import kong.unirest.Unirest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CoffeeMachineServiceImpl implements CoffeeMachineService {
    @Autowired
    private CoffeeMachineRepository coffeeMachineRepository;
    @Autowired
    private ProductRepository productRepository;

    @Override
    public CoffeeMachine save(CoffeeMachine coffeeMachine) {
        List<CoffeeMachine> coffeeMachines = (List<CoffeeMachine>) coffeeMachineRepository.findAll();
        boolean exists = coffeeMachines
                .stream()
                .anyMatch(c -> c.getPort().equals(coffeeMachine.getPort())
                        && c.getUrl().equals(coffeeMachine.getUrl()));

        if (exists) {
            throw new CoffeeMachineAlreadyExistsException("Une machine à café avec l'url "
                    + coffeeMachine.getUrl() + ":" + coffeeMachine.getPort() + " existe déjà");
        }
        coffeeMachine.setActive(false);
        return coffeeMachineRepository.save(coffeeMachine);
    }

    @Override
    public CoffeeMachine findById(Long id) {
        return coffeeMachineRepository
                        .findById(id)
                        .orElseThrow(() -> new CoffeeMachineNotFoundException(
                                "Aucune m'achine à café d'identifiant " + id + " existe"));
    }

    @Override
    public CoffeeMachine findByUrlAndPort(String url, Integer port) {
        Optional<CoffeeMachine> optionalCoffeeMachine = coffeeMachineRepository
                .findByUrlAndPort(url, port);

        if (!optionalCoffeeMachine.isPresent()) {
            return null;
        }
        return optionalCoffeeMachine.get();
    }


    @Override
    public Iterable<CoffeeMachine> findAll() {
        return coffeeMachineRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        Optional<CoffeeMachine> oCoffeeMachine = coffeeMachineRepository.findById(id);
        if (!oCoffeeMachine.isPresent()) {
            throw new CoffeeMachineNotFoundException("Aucune machine à café d'identifiant " + id);
        }
        shutdown(oCoffeeMachine.get());
        coffeeMachineRepository.delete(oCoffeeMachine.get());
    }

    @Override
    public CoffeeMachine update(Long id, CoffeeMachine coffeeMachine) {
        List<CoffeeMachine> coffeeMachines = (List<CoffeeMachine>) coffeeMachineRepository.findAll();
        boolean exists = coffeeMachines
                .stream()
                .anyMatch(c -> c.getPort().equals(coffeeMachine.getPort())
                        && c.getUrl().equals(coffeeMachine.getUrl())
                        && !c.getId().equals(id));

        if (exists) {
            throw new CoffeeMachineAlreadyExistsException("Une machine à café avec l'url "
                    + coffeeMachine.getUrl() + ":" + coffeeMachine.getPort() + " existe déjà");
        }

        return coffeeMachineRepository.findById(id)
                .map(x -> {
                    x.setUrl(coffeeMachine.getUrl());
                    x.setPort(coffeeMachine.getPort());
                    return coffeeMachineRepository.save(x);
                })
                .orElseThrow(() -> new CoffeeMachineNotFoundException(
                        "Aucune machine à café de port " + id + " existe"));
    }

    @Override
    public CoffeeMachine updateStatus(Long id, boolean status) {
        return coffeeMachineRepository.findById(id)
                .map(x -> {
                    x.setActive(status);
                    Iterable<Product> products = productRepository.findAllByCoffeeMachine(x);
                    boolean isOutOfStock = false;
                    for (Product p : products) {
                        if (p.getQuantity().equals(0)) {
                            isOutOfStock = true;
                        }
                    }
                    x.setOutOfStock(isOutOfStock);
                    return coffeeMachineRepository.save(x);
                })
                .orElseThrow(() -> new CoffeeMachineNotFoundException(
                        "Aucune machine à café de port " + id + " existe"));
    }

    @Override
    public CoffeeMachine updateVersion(Long id, String version) {
        return coffeeMachineRepository.findById(id)
                .map(x -> {
                    if (version == null) {
                        x.setVersion("1.0");
                    } else {
                        x.setVersion(version);
                    }
                    return coffeeMachineRepository.save(x);
                })
                .orElseThrow(() -> new CoffeeMachineNotFoundException(
                        "Aucune machine à café de port " + id + " existe"));
    }

    @Override
    public void shutdown(CoffeeMachine coffeeMachine) {
        String url = coffeeMachine.getUrl() + ":" + coffeeMachine.getPort() + "/shutdown";
        try {
            Unirest.post(url).asJson();
        } catch (Exception e) { }
    }
}