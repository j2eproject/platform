package com.coffee.backoffice.service;

import com.coffee.backoffice.model.CoffeeMachine;

public interface CoffeeMachineService {
    CoffeeMachine save(CoffeeMachine user);

    CoffeeMachine findById(Long id);

    CoffeeMachine findByUrlAndPort(String url, Integer port);

    Iterable<CoffeeMachine> findAll();

    void delete(Long id);

    CoffeeMachine update(Long id, CoffeeMachine user);

    CoffeeMachine updateStatus(Long id, boolean status);

    CoffeeMachine updateVersion(Long id, String version);

    void shutdown(CoffeeMachine coffeeMachine);

}

