package com.coffee.backoffice.service;

import com.coffee.backoffice.model.CoffeeMachine;
import com.coffee.backoffice.model.Product;
import com.coffee.backoffice.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;


public interface ProductService {
    Product save(Product product);

    Product saveData(Product product);

    Product findById(Long id);

    Iterable<Product> findAll();

    Iterable<Product> findAllByCoffeeMachine(CoffeeMachine coffeeMachine);

    void delete(String libelle, int quantity, CoffeeMachine coffeeMachine);

    long deleteAllByCoffeeMachine(CoffeeMachine coffeeMachine);

}

