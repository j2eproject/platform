package com.coffee.backoffice;

import com.coffee.backoffice.model.User;
import com.coffee.backoffice.service.AuthService;
import com.coffee.backoffice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    UserService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf()
                .disable()
                .exceptionHandling()
                .authenticationEntryPoint(new Http403ForbiddenEntryPoint() {
                })
                .and()
                    .httpBasic()
                .and()
                    .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                    .authorizeRequests()
                    .antMatchers(HttpMethod.GET, "/api/users/**").hasRole("ADMIN")
                    .antMatchers(HttpMethod.POST, "/api/users/**").permitAll()
                    .antMatchers(HttpMethod.PUT, "/api/users/**").hasAnyRole("USER", "ADMIN")
                    .antMatchers(HttpMethod.PATCH, "/api/users/**").hasAnyRole("USER", "ADMIN")
                    .antMatchers(HttpMethod.DELETE, "/api/users/**").hasRole("ADMIN")
                    .antMatchers(HttpMethod.GET, "/api/coffeemachines").hasAnyRole("USER", "ADMIN")
                    .antMatchers(HttpMethod.GET, "/api/coffeemachines/{\\d+}").hasAnyRole("USER", "ADMIN")
                    .antMatchers(HttpMethod.GET, "/api/coffeemachines/{\\d+}/products").hasAnyRole("USER", "ADMIN")
                    .antMatchers(HttpMethod.GET, "/api/coffeemachines/{\\d+}/products/csvreport").hasRole("ADMIN")
                    .antMatchers(HttpMethod.GET, "/api/coffeemachines/{\\d+}/products/jsonreport").hasRole("ADMIN")
                    .antMatchers(HttpMethod.GET, "/api/coffeemachines/{\\d+}/products/pdfreport").hasRole("ADMIN")
                    .antMatchers(HttpMethod.POST, "/api/coffeemachines").hasRole("ADMIN")
                    .antMatchers(HttpMethod.POST, "/api/coffeemachines/{\\d+}/products").hasAnyRole("USER", "ADMIN")
                    .antMatchers(HttpMethod.PUT, "/api/coffeemachines/**").hasRole("ADMIN")
                    .antMatchers(HttpMethod.DELETE, "/api/coffeemachines/**").hasAnyRole("USER", "ADMIN")

                .antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources",
                            "/configuration/security", "/swagger-ui.html", "/webjars/**",
                            "/swagger-resources/configuration/ui", "/swagge‌​r-ui.html",
                            "/swagger-resources/configuration/security").permitAll()
                    .antMatchers("/api/authenticate").permitAll();

    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}