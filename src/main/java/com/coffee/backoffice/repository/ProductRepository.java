package com.coffee.backoffice.repository;

import com.coffee.backoffice.model.CoffeeMachine;
import com.coffee.backoffice.model.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends CrudRepository<Product, Long> {
    Optional<Product> findByLibelle(String libelle);
    Iterable<Product> findAllByCoffeeMachine(CoffeeMachine coffeeMachine);
    long deleteAllByCoffeeMachine(CoffeeMachine coffeeMachine);
    void deleteByLibelle(String libelle);
}