package com.coffee.backoffice.repository;

import com.coffee.backoffice.model.CoffeeMachine;
import com.coffee.backoffice.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CoffeeMachineRepository extends CrudRepository<CoffeeMachine, Long> {
    Optional<CoffeeMachine> findByUrlAndPort(String url, Integer port);
}