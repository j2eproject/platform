package com.coffee.backoffice.controller;

import com.coffee.backoffice.dto.*;
import com.coffee.backoffice.exception.CoffeeMachineAlreadyExistsException;
import com.coffee.backoffice.exception.CoffeeMachineNotFoundException;
import com.coffee.backoffice.model.CoffeeMachine;
import com.coffee.backoffice.model.Product;
import com.coffee.backoffice.service.CoffeeMachineService;
import com.coffee.backoffice.service.ProductService;
import com.coffee.backoffice.util.CsvGenerator;
import com.coffee.backoffice.util.PdfGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/api/coffeemachines")
public class CoffeeMachineController {
    @Autowired
    private CoffeeMachineService coffeeMachineService;

    @Autowired
    private ProductService productService;

    @Autowired
    private ModelMapper modelMapper;

    @ApiOperation(value = "Recupère la liste des machines à cafés", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "La récupération de la liste s'est  correctement déroulée"),
            @ApiResponse(code = 401, message = "Accès non autorisé (nécessite d'être authentifié)"),
            @ApiResponse(code = 403, message = "Accès interdit (nécessite d'être authentifié)"),
            @ApiResponse(code = 404, message = "Ressource non trouvée")
    })
    @GetMapping(produces = "application/json")
    public List<CoffeeMachineDTO> findAll() {
        return toDTO(coffeeMachineService.findAll());
    }

    @ApiOperation(value = "Créé une nouvelle machine à café", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "La création s'est  correctement déroulée"),
            @ApiResponse(code = 400, message = "Body de la requête mal formé"),
            @ApiResponse(code = 401, message = "Accès non autoris (nécessite les droits admin)"),
            @ApiResponse(code = 403, message = "Accès interdit (nécessite les droits admin)"),
            @ApiResponse(code = 404, message = "Ressource non trouvée"),
            @ApiResponse(code = 409, message = "La machine à café existe déjà"),
    })
    @PostMapping()
    public ResponseEntity<?> newUser(@Valid @RequestBody CoffeeMachineDTO dto) {
        try {
            coffeeMachineService.save(modelMapper.map(dto, CoffeeMachine.class));
        } catch (CoffeeMachineAlreadyExistsException e) {
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @ApiOperation(value = "Recupère une machine a café", response = CoffeeMachine.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "La récupération s'est  correctement déroulée"),
            @ApiResponse(code = 401, message = "Accès non autorisé (nécessite d'être authentifié)"),
            @ApiResponse(code = 403, message = "Accès interdit (nécessite d'être authentifié)"),
            @ApiResponse(code = 404, message = "Ressource non trouvée")
    })
    @GetMapping(path="/{id}", produces = "application/json")
    public CoffeeMachineDTO findOne(@PathVariable Long id) {
        return toDTO(coffeeMachineService.findById(id));
    }

    @ApiOperation(value = "Modifie une machine à café", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "La modification s'est  correctement déroulée"),
            @ApiResponse(code = 400, message = "Body de la requête mal formé"),
            @ApiResponse(code = 401, message = "Accès non autorisé (nécessite les droits admin)"),
            @ApiResponse(code = 403, message = "Accès interdit (nécessite les droits admin)"),
            @ApiResponse(code = 404, message = "Ressource non trouvée")
    })
    @PutMapping(path="/{id}", produces = "application/json")
    public CoffeeMachineDTO updateCoffeeMachine(@Valid @RequestBody EditCoffeeMachineDTO dto, @PathVariable Long id) {
        return toDTO(coffeeMachineService.update(id, modelMapper.map(dto, CoffeeMachine.class)));
    }

    @ApiOperation(value = "Supprime une machine à café")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "La suppression s'est  correctement déroulée"),
            @ApiResponse(code = 401, message = "Accès non autorisé (nécessite les droits admin ou utilisateur)"),
            @ApiResponse(code = 403, message = "Accès interdit (nécessite les droits admin ou utilisateur)"),
            @ApiResponse(code = 404, message = "Ressource non trouvée")
    })
    @DeleteMapping(path="/{id}", produces = "application/json")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteCoffeeMachine(@PathVariable Long id) {
        coffeeMachineService.delete(id);
    }

    @ApiOperation(value = "Créé un nouveau produit", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "La création s'est  correctement déroulée"),
            @ApiResponse(code = 400, message = "Body de la requête mal formé"),
            @ApiResponse(code = 401, message = "Accès non autorisé (nécessite les droits admin ou user)"),
            @ApiResponse(code = 403, message = "Accès interdit (nécessite les droits admin ou user)"),
            @ApiResponse(code = 404, message = "Ressource non trouvée"),
    })
    @PostMapping(path="/{id}/products", produces = "application/json")
    public ResponseEntity<?> addProduct(@PathVariable Long id, @Valid @RequestBody CreateProductDTO dto) {
        CoffeeMachine coffeeMachine = null;
        try {
            coffeeMachine = coffeeMachineService.findById(id);
        } catch (CoffeeMachineNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }

        try {
            if (coffeeMachine.isActive()) {
                Product p = modelMapper.map(dto, Product.class);
                p.setCoffeeMachine(coffeeMachine);
                productService.save(p);
                coffeeMachineService.updateStatus(coffeeMachine.getId(), true);
            } else {
                return new ResponseEntity<>("Impossible d'ajouter des produits à une machine éteinte",
                        HttpStatus.BAD_REQUEST);
            }
        } catch (CoffeeMachineNotFoundException e) {
            coffeeMachineService.updateStatus(coffeeMachine.getId(), false);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @ApiOperation(value = "Supprime un produit")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "La suppression s'est  correctement déroulée"),
            @ApiResponse(code = 401, message = "Accès non autorisé (nécessite les droits admin ou utilisateur)"),
            @ApiResponse(code = 403, message = "Accès interdit (nécessite les droits admin ou utilisateur)"),
            @ApiResponse(code = 404, message = "Ressource non trouvée")
    })
    @DeleteMapping(path="/{id}/products/{libelle}", produces = "application/json")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    @Transactional
    public ResponseEntity<?> deleteProduct(@PathVariable Long id, @PathVariable String libelle, @Valid @RequestBody DeleteProductDTO dto) {
        CoffeeMachine coffeeMachine = null;
        try {
            coffeeMachine = coffeeMachineService.findById(id);
        } catch (CoffeeMachineNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }

        try {
            if (coffeeMachine.isActive()) {
                productService.delete(libelle, dto.getQuantity(), coffeeMachine);
                coffeeMachineService.updateStatus(coffeeMachine.getId(), true);
            } else {
                return new ResponseEntity<>("Impossible de supprimer des produits  d'une machine éteinte",
                        HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            coffeeMachineService.updateStatus(id, false);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }


    @ApiOperation(value = "Recupère la liste des produits associés à une machine", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "La récupération de la liste s'est  correctement déroulée"),
            @ApiResponse(code = 401, message = "Accès non autorisé (nécessite d'être authentifié)"),
            @ApiResponse(code = 403, message = "Accès interdit (nécessite d'être authentifié)"),
            @ApiResponse(code = 404, message = "Ressource non trouvée")
    })
    @GetMapping(path = "/{id}/products" , produces = "application/json")
    public List<ProductDTO> findProductsForCoffeeMachine(@PathVariable Long id) {
        return toDTOProducts(productService.findAllByCoffeeMachine(coffeeMachineService.findById(id)));
    }

    @ApiOperation(value = "Recupère la liste des produits d'une machine à café au format PDF")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "La récupération du pdf s'est  correctement déroulée"),
            @ApiResponse(code = 401, message = "Accès non autorisé (nécessite les droits admin)"),
            @ApiResponse(code = 403, message = "Accès interdit (nécessite les droits admin)"),
            @ApiResponse(code = 404, message = "Ressource non trouvée")
    })
    @GetMapping(path = "/{id}/products/pdfreport")
    public ResponseEntity<InputStreamResource> pdfReport(@PathVariable Long id)
    {
        List<ProductDTO> productDTOS = toDTOProducts(
                productService.findAllByCoffeeMachine(coffeeMachineService.findById(id)));
        return ResponseEntity
                .ok()
                .headers(getHeaders(".pdf", MediaType.valueOf("application/pdf")))
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(PdfGenerator.productsReport(productDTOS)));
    }

    @ApiOperation(value = "Recupère la liste des produits d'une machine à café au format CSV")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "La récupération du csv s'est  correctement déroulée"),
            @ApiResponse(code = 401, message = "Accès non autorisé (nécessite les droits admin)"),
            @ApiResponse(code = 403, message = "Accès interdit (nécessite les droits admin)"),
            @ApiResponse(code = 404, message = "Ressource non trouvée")
    })
    @GetMapping(value = "/{id}/products/csvreport")
    public void csvReport(HttpServletResponse response, @PathVariable Long id) throws IOException {
        response.setHeader("Content-Disposition", "attachment; filename=products_report.csv");
        response.setContentType("text/csv");
        List<ProductDTO> productDTOS = toDTOProducts(
                productService.findAllByCoffeeMachine(coffeeMachineService.findById(id)));
        CsvGenerator.productsReport(response.getWriter(), productDTOS);
    }

    @ApiOperation(value = "Recupère la liste des produits d'une machine à café au format JSON")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "La récupération du fichier JSON s'est  correctement déroulée"),
            @ApiResponse(code = 401, message = "Accès non autorisé (nécessite les droits admin)"),
            @ApiResponse(code = 403, message = "Accès interdit (nécessite les droits admin)"),
            @ApiResponse(code = 404, message = "Ressource non trouvée")
    })
    @GetMapping("/{id}/products/jsonreport")
    public ResponseEntity<?> jsonReport(@PathVariable Long id) throws Exception {
        List<ProductDTO> productDTOS = toDTOProducts(
                productService.findAllByCoffeeMachine(coffeeMachineService.findById(id)));
        return new ResponseEntity<>(
                new ObjectMapper().writeValueAsString(productDTOS).getBytes(),
                getHeaders(".json",new MediaType("text", "json")),
                HttpStatus.OK);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    //                                  Méthodes privées                                    //
    /////////////////////////////////////////////////////////////////////////////////////////

    private HttpHeaders getHeaders(String extension, MediaType mediaType) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=products_result" + extension);
        headers.setContentType(mediaType);
        return headers;
    }

    private List<CoffeeMachineDTO> toDTO(Iterable<CoffeeMachine> coffeeMachines) {
        List<CoffeeMachineDTO> coffeeMachineDto = new ArrayList<>();
        for (CoffeeMachine cm : coffeeMachines) {
            coffeeMachineDto.add(toDTO(cm));
        }
        return coffeeMachineDto;
    }


    private CoffeeMachineDTO toDTO(CoffeeMachine coffeeMachine) {
        return modelMapper.map(coffeeMachine, CoffeeMachineDTO.class);
    }

    private List<ProductDTO> toDTOProducts(Iterable<Product> products) {
        List<ProductDTO> productDTOS = new ArrayList<>();
        for (Product p : products) {
            productDTOS.add(toDTO(p));
        }
        return productDTOS;
    }


    private ProductDTO toDTO(Product product) {
        ProductDTO p = modelMapper.map(product, ProductDTO.class);
        p.setCoffeemachine(product.getCoffeeMachine().getUrl() + ":" + product.getCoffeeMachine().getPort());
        return p;
    }
}
