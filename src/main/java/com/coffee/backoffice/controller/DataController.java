package com.coffee.backoffice.controller;

import com.coffee.backoffice.dto.*;
import com.coffee.backoffice.exception.CoffeeMachineAlreadyExistsException;
import com.coffee.backoffice.exception.CoffeeMachineNotFoundException;
import com.coffee.backoffice.model.CoffeeMachine;
import com.coffee.backoffice.model.Product;
import com.coffee.backoffice.repository.ProductRepository;
import com.coffee.backoffice.service.CoffeeMachineService;
import com.coffee.backoffice.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.Map;

@RestController
@RequestMapping(path = "/data")
public class DataController {
    private static final Logger logger = LoggerFactory.getLogger(DataController.class);

    @Autowired
    private ProductService productService;
    @Autowired
    private CoffeeMachineService coffeeMachineService;

    @Transactional
    @PostMapping(produces = "application/json")
    public void data(HttpServletRequest request, @RequestBody DataDTO dto) {
        CoffeeMachine coffeeMachine = coffeeMachineService.findByUrlAndPort(
               "http://" + request.getRemoteAddr(),  dto.getPort());
        if (coffeeMachine == null) {
            logger.info("Cette machine a café n'existe pas dans la plateforme !");
            throw new CoffeeMachineNotFoundException("Aucune machine à café avec "
                    + request.getRemoteAddr() + ":" + dto.getPort());
        } else {
            productService.deleteAllByCoffeeMachine(coffeeMachine);
            logger.info("Mise à jour des produits !");
            for (Map.Entry<String, Object> current : dto.getProducts()) {
                Product product = new Product();
                product.setLibelle(current.getKey());
                // Version 1.1
                if (coffeeMachine.getVersion().equals("1.1")) {
                    product.setQuantity(Integer.parseInt(current.getValue().toString()));
                } else { // Version 1.0
                    if (current.getValue() instanceof Integer){
                        product.setQuantity(Math.abs(Integer.parseInt(current.getValue().toString())));
                    } else {
                        product.setQuantity(0);
                    }
                }

                product.setCoffeeMachine(coffeeMachine);
                productService.saveData(product);
            }
            coffeeMachineService.updateVersion(coffeeMachine.getId(), dto.getVersion());
            coffeeMachineService.updateStatus(coffeeMachine.getId(), true);
        }
    }
}
