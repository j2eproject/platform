package com.coffee.backoffice.controller;

import com.coffee.backoffice.dto.EditPasswordDTO;
import com.coffee.backoffice.dto.EditUserDTO;
import com.coffee.backoffice.dto.RegisterDTO;
import com.coffee.backoffice.dto.UserDTO;
import com.coffee.backoffice.exception.UserAlreadyExistsException;
import com.coffee.backoffice.exception.UserForbiddenException;
import com.coffee.backoffice.model.User;
import com.coffee.backoffice.service.UserService;
import com.coffee.backoffice.util.CsvGenerator;
import com.coffee.backoffice.util.PdfGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/api/users")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @ApiOperation(value = "Recupère la liste des utilisateurs", response = List.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "La récupération de la liste s'est  correctement déroulée"),
        @ApiResponse(code = 401, message = "Accès non autorisé (nécessite les droits admin)"),
        @ApiResponse(code = 403, message = "Accès interdit (nécessite les droits admin)"),
        @ApiResponse(code = 404, message = "Ressource non trouvée")
    })
    @GetMapping(produces = "application/json")
    public List<UserDTO> findAll()
    {
        return toDTO(userService.findAll());
    }

    @ApiOperation(value = "Créé un nouvel utilisateur", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "La création s'est  correctement déroulée"),
            @ApiResponse(code = 400, message = "Body de la requête mal formé"),
            @ApiResponse(code = 401, message = "Accès non autorisé"),
            @ApiResponse(code = 403, message = "Accès interdit"),
            @ApiResponse(code = 404, message = "Ressource non trouvée"),
            @ApiResponse(code = 409, message = "L'utilisateur existe déjà"),
    })
    @PostMapping()
    public ResponseEntity<?> newUser(@Valid @RequestBody RegisterDTO user) {
        if (user.password.equals(user.confirmPassword)) {
            try {
                userService.save(modelMapper.map(user, User.class));
            } catch (UserAlreadyExistsException e) {
                return new ResponseEntity<Object>(e.getMessage(), HttpStatus.CONFLICT);
            }
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>("Les mots de passe ne correspondent pas", HttpStatus.BAD_REQUEST);
    }

    @ApiOperation(value = "Recupère un utilisateur", response = UserDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "La récupération s'est  correctement déroulée"),
            @ApiResponse(code = 401, message = "Accès non autorisé (nécessite les droits admin)"),
            @ApiResponse(code = 403, message = "Accès interdit (nécessite les droits admin)"),
            @ApiResponse(code = 404, message = "Ressource non trouvée")
    })
    @GetMapping(path="/{id}", produces = "application/json")
    public UserDTO findOne(@PathVariable Long id) {
        return toDTO(userService.findById(id));
    }

    @ApiOperation(value = "Modifie un utilisateur (le rôle peut être modifié par l'admin)", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "La modification s'est  correctement déroulée"),
            @ApiResponse(code = 400, message = "Body de la requête mal formé"),
            @ApiResponse(code = 401, message = "Accès non autorisé (nécessite les droits admin ou user courant)"),
            @ApiResponse(code = 403, message = "Accès interdit (nécessite les droits admin ou user courant)"),
            @ApiResponse(code = 404, message = "Ressource non trouvée")
    })
    @PutMapping(path="/{id}", produces = "application/json")
    public UserDTO updateUser(@Valid @RequestBody EditUserDTO dto, @PathVariable Long id) {
        if (isAdmin()) {
            return toDTO(userService.updateWithRole(id, toEntity(dto)));
        } else if (userService.findByMail(username()).getId().equals(id)) {
            return toDTO(userService.update(id, toEntity(dto)));
        }
        throw new UserForbiddenException("Vous ne pouvez pas modifier l'utilisateur d'identifiant " + id);
    }

    @ApiOperation(value = "Modifie le mot de passe de l'utilisateur", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "La modification s'est  correctement déroulée"),
            @ApiResponse(code = 400, message = "Body de la requête mal formé"),
            @ApiResponse(code = 401, message = "Accès non autorisé (nécessite d'être user courant)"),
            @ApiResponse(code = 403, message = "Accès interdit (nécessite d'être user courant)"),
            @ApiResponse(code = 404, message = "Ressource non trouvée")
    })
    @PatchMapping(path="/{id}", produces = "application/json")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void patchUser(@Valid @RequestBody EditPasswordDTO dto, @PathVariable Long id) {
        if (userService.findByMail(username()).getId().equals(id)) {
            userService.updatePassword(id, dto.getPassword(), dto.getConfirmPassword());
        } else {
            throw new UserForbiddenException("Vous ne pouvez pas modifier l'utilisateur d'identifiant " + id);
        }
    }

    @ApiOperation(value = "Supprime un utilisateur")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "La suppression s'est  correctement déroulée"),
            @ApiResponse(code = 401, message = "Accès non autorisé (nécessite les droits admin)"),
            @ApiResponse(code = 403, message = "Accès interdit (nécessite les droits admin)"),
            @ApiResponse(code = 404, message = "Ressource non trouvée")
    })
    @DeleteMapping(path="/{id}", produces = "application/json")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable Long id) {
        userService.delete(id);
    }

    @ApiOperation(value = "Recupère la liste des utilisateurs au format PDF")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "La récupération du pdf s'est  correctement déroulée"),
            @ApiResponse(code = 401, message = "Accès non autorisé (nécessite les droits admin)"),
            @ApiResponse(code = 403, message = "Accès interdit (nécessite les droits admin)"),
            @ApiResponse(code = 404, message = "Ressource non trouvée")
    })
    @GetMapping(path = "/pdfreport")
    public ResponseEntity<InputStreamResource> pdfReport()
    {
        List<UserDTO> usersDto = toDTO(userService.findAll());
        return ResponseEntity
                .ok()
                .headers(getHeaders(".pdf", MediaType.valueOf("application/pdf")))
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(PdfGenerator.usersReport(usersDto)));
    }

    @ApiOperation(value = "Recupère la liste des utilisateurs au format CSV")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "La récupération du csv s'est  correctement déroulée"),
            @ApiResponse(code = 401, message = "Accès non autorisé (nécessite les droits admin)"),
            @ApiResponse(code = 403, message = "Accès interdit (nécessite les droits admin)"),
            @ApiResponse(code = 404, message = "Ressource non trouvée")
    })
    @GetMapping(value = "/csvreport")
    public void csvReport(HttpServletResponse response) throws IOException {
        response.setHeader("Content-Disposition", "attachment; filename=users_report.csv");
        response.setContentType("text/csv");
        CsvGenerator.usersReport(response.getWriter(), toDTO(userService.findAll()));
    }

    @ApiOperation(value = "Recupère la liste des utilisateurs au format JSON")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "La récupération du fichier JSON s'est  correctement déroulée"),
            @ApiResponse(code = 401, message = "Accès non autorisé (nécessite les droits admin)"),
            @ApiResponse(code = 403, message = "Accès interdit (nécessite les droits admin)"),
            @ApiResponse(code = 404, message = "Ressource non trouvée")
    })
    @GetMapping("/jsonreport")
    public ResponseEntity<?> jsonReport() throws Exception {
        return new ResponseEntity<>(
                new ObjectMapper().writeValueAsString(toDTO(userService.findAll())).getBytes(),
                getHeaders(".json",new MediaType("text", "json")),
                HttpStatus.OK);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    //                                  Méthodes privées                                    //
    /////////////////////////////////////////////////////////////////////////////////////////

    private HttpHeaders getHeaders(String extension, MediaType mediaType) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=users_result" + extension);
        headers.setContentType(mediaType);
        return headers;
    }

    private List<UserDTO> toDTO(Iterable<User> users) {
        List<UserDTO> usersDto = new ArrayList<>();
        for (User u : users) {
            usersDto.add(toDTO(u));
        }
        return usersDto;
    }

    private User toEntity(EditUserDTO dto) {
        return modelMapper.map(dto, User.class);
    }

    private UserDTO toDTO(User user) {
        return modelMapper.map(user, UserDTO.class);
    }

    private boolean isAdmin() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities()
                .stream()
                .findFirst()
                .get().toString().equals("ROLE_ADMIN");
    }

    private String username() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

}