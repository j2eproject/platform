package com.coffee.backoffice.controller;

import com.coffee.backoffice.dto.LoginDTO;
import com.coffee.backoffice.dto.UserDTO;
import com.coffee.backoffice.model.User;
import com.coffee.backoffice.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/authenticate")
public class AuthenticateController {
    @Autowired
    private UserService userService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping()
    public ResponseEntity<Object> authenticate(@Valid @RequestBody LoginDTO login) {
        User user = userService.findByMail(login.getMail());

        if (user == null || !passwordEncoder.matches(login.getPassword(), user.getPassword())) {
            return new ResponseEntity<Object>("Le couple mail / mot de passe ne correspond pas pour " +
                    "l'utilisateur : " + user.getMail(), HttpStatus.BAD_REQUEST);

        }
        return new ResponseEntity<Object>(modelMapper.map(user, UserDTO.class), HttpStatus.OK);
    }

}